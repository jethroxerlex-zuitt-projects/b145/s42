// console.log('hi')

console.log(document);

// document.querySelector('#txt-first-name');

//alternatively

// document.getElementById('txt-first-name');

//for other types

	//document.getElementByClassName('class-name')
	//document.getElementByTagName('tag-name')

// Contain the query selector code in a constant

// const txtFirstName = document.querySelector("#txt-first-name");	

// const spanFullName = document.querySelector("#span-full-name");

// Event Listeners

// txtFirstName.addEventListener('keyup',(event) => {
// 	spanFullName.innerHTML = txtFirstName.value;

// })


// //Multiple Listeners
// 	//e is a shorthand for event


// txtFirstName.addEventListener('keyup', (e) => {
// 	console.log(e.target);
// 	console.log(e.target.value);
// });


//ACTIVITY

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

txtFirstName.addEventListener('keyup',(event) => {
	spanFullName.innerHTML = txtFirstName.value 
})

txtLastName.addEventListener('keyup',(event2) => {
	spanFullName.innerHTML = `${txtFirstName.value}  ${txtLastName.value}`
})